package br.com.kaiquer.person.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "O nome do cliente não pode estar em branco")
public class NameNullException extends RuntimeException {
}
