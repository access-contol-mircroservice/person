package br.com.kaiquer.person.models.dto;

import br.com.kaiquer.person.models.Person;
import br.com.kaiquer.person.models.dto.request.PersonCreateRequest;
import br.com.kaiquer.person.models.dto.response.PersonCreatedResponse;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper {

    public Person toPerson (PersonCreateRequest personCreateRequest) {
        Person person = new Person();
        person.setName(personCreateRequest.getName());

        return person;
    }

    public PersonCreatedResponse toPersonCreatedResponse(Person person) {
        PersonCreatedResponse personCreatedResponse = new PersonCreatedResponse();
        personCreatedResponse.setId(person.getId());
        personCreatedResponse.setName(person.getName());

        return personCreatedResponse;
    }
}
