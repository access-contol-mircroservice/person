package br.com.kaiquer.person.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonCreateRequest {
    @JsonProperty(value = "nome")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
