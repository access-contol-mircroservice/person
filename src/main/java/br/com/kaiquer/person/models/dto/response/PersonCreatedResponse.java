package br.com.kaiquer.person.models.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonCreatedResponse {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "nome")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
