package br.com.kaiquer.person.services;

import br.com.kaiquer.person.exceptions.NameNullException;
import br.com.kaiquer.person.exceptions.PersonNotFoundException;
import br.com.kaiquer.person.models.Person;
import br.com.kaiquer.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public Person create(Person person) {
        if(person.getName() == null) {
            throw new NameNullException();
        }
        return personRepository.save(person);
    }

    public Person findById(Long id) {
        Optional<Person> person = personRepository.findById(id);
        if(!person.isPresent()) {
            throw new PersonNotFoundException();
        }
        return person.get();
    }
}
