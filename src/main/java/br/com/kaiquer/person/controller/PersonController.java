package br.com.kaiquer.person.controller;

import br.com.kaiquer.person.models.Person;
import br.com.kaiquer.person.models.dto.PersonMapper;
import br.com.kaiquer.person.models.dto.request.PersonCreateRequest;
import br.com.kaiquer.person.models.dto.response.PersonCreatedResponse;
import br.com.kaiquer.person.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class PersonController {
    @Autowired
    private PersonService personService;
    @Autowired
    private PersonMapper mapper;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    private PersonCreatedResponse newPerson(@RequestBody PersonCreateRequest personCreateRequest) {
        Person person = mapper.toPerson(personCreateRequest);
        person = personService.create(person);
        return mapper.toPersonCreatedResponse(person);
    }

    @GetMapping("/{id}")
    private PersonCreatedResponse findById(@PathVariable(value = "id") Long id) {
        Person person = personService.findById(id);
        return mapper.toPersonCreatedResponse(person);
    }

}
