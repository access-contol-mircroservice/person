package br.com.kaiquer.person.repository;

import br.com.kaiquer.person.models.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
